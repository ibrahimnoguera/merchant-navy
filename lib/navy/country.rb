class Country < ActiveRecord::Base
  validates_uniqueness_of :name
  validates_uniqueness_of :acronym
end
