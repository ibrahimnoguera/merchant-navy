class Consignee < ActiveRecord::Base
  validates_uniqueness_of :name
  validates_presence_of :country_id, :unless => :private_client_id?
  validates_presence_of :private_client_id, :unless => :country_id?
  validate :any_present?
  def any_present?
    if %w(country_id private_client_id).all?{|attr| self[attr].blank?}
      errors.add(:private_client_id, "Either country or private client must be filled")
    end
  end
end
