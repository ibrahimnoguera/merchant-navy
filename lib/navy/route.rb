class Route < ActiveRecord::Base
  validates_uniqueness_of :port_of_loading_id, :scope => :port_of_discharge_id
  validate :vary_route
  belongs_to :loading, class_name:'Port', foreign_key: :port_of_loading_id
  belongs_to :discharge, class_name:'Port', foreign_key: :port_of_discharge_id
  def vary_route
    errors.add(:port_of_discharge_id, 'Route start and end must be different') if self.port_of_loading_id === self.port_of_discharge_id
  end
end
