class Container < ActiveRecord::Base
  validates_uniqueness_of :code
  validates :code, length: { is: 15 }
  validate :valid_code
  validates_presence_of :country_id, :unless => :nvocc_id?
  validates_presence_of :nvocc_id, :unless => :country_id?
  validate :any_present?
  has_and_belongs_to_many :cargos
  has_and_belongs_to_many :bl_masters
  has_and_belongs_to_many :bl_houses
  def any_present?
    if %w(country_id nvocc_id).all?{|attr| self[attr].blank?}
      errors.add(:nvocc_id, "Either country or nvooc must be filled")
    end
  end
  def valid_code
    upc = ('A'..'Z').to_a
    num=(0.to_s..9.to_s).to_a
    c=code.split('')
    if code.length!=15 or !upc.include?(c[0]) or !upc.include?(c[1]) or !upc.include?(c[2]) or !upc.include?(c[3]) or !num.include?(c[4]) or !num.include?(c[5]) or !num.include?(c[6]) or !num.include?(c[7]) or !num.include?(c[8]) or !num.include?(c[9]) or !num.include?(c[10]) or !num.include?(c[11]) or !num.include?(c[12]) or !num.include?(c[13]) or !num.include?(c[14]) then
      errors.add(:code, "Wrong code")
    end
  end
end
