class User < ActiveRecord::Base
  validates_uniqueness_of :e_mail
  validates_uniqueness_of :national_id_number
end
