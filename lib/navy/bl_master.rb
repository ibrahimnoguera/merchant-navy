class BlMaster < ActiveRecord::Base
  validates_uniqueness_of :bl_code
  belongs_to :notify, class_name:'User', foreign_key: :notify_id
  belongs_to :loading, class_name:'Port', foreign_key: :port_of_loading_id
  belongs_to :discharge, class_name:'Port', foreign_key: :port_of_discharge_id
  has_and_belongs_to_many :containers
end
