class BlHouse < ActiveRecord::Base
  validates_uniqueness_of :bl_code
  has_and_belongs_to_many :containers
end
