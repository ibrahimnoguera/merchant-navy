class Port < ActiveRecord::Base
  validates_uniqueness_of :port_code
end
