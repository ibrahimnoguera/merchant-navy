class PrivateClient < ActiveRecord::Base
  validates_uniqueness_of :company_name
  belongs_to :owner, class_name:'User', foreign_key: :user_id
  has_and_belongs_to_many :nvoccs
end
