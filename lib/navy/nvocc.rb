class Nvocc < ActiveRecord::Base
  validates_uniqueness_of :name
  validates_uniqueness_of :license_number
  validates_uniqueness_of :phone
  has_and_belongs_to_many :private_clients
end
