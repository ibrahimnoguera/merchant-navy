class Login < ActiveRecord::Base
  validates_uniqueness_of :email
  validate :valid_mail

  def valid_mail
    if !email.include? ('@') or !email.include? ('.com') then
      errors.add(:email, "Not a valid email")
    end
  end
end
