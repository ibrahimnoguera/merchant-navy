require_relative '../db/connection'
require_relative 'navy/country'
require_relative 'navy/port'
require_relative 'navy/route'
require_relative 'navy/user'
require_relative 'navy/nvocc'
require_relative 'navy/private_client'
require_relative 'navy/consignee'
require_relative 'navy/shipping_company'
require_relative 'navy/cargo_ship'
require_relative 'navy/container'
require_relative 'navy/bl_master'
require_relative 'navy/cargo'
require_relative 'navy/bl_house'

class Navy
  def user_interface
    system('clear')
    puts 'Bienvenido a NavySearch 2.0.1'
    puts '1-Nuevo Usuario'
    puts '2-Login'
    puts '3-Salir'
    ops = gets.chomp.to_i
    system('clear')
    case ops
    when 1
      system('clear')
      puts 'Nuevo Usuario'
      login = Login.new
      puts 'Ingrese correo'
      login.email = gets.chomp.to_s
      puts 'Ingrese clave'
      login.password = gets.chomp.to_s
      login.save
      puts 'Confirme clave'
      l = gets.chomp.to_s
      while l!=login.password do
        system('clear')
        puts 'Clave incorrecta, intente de nuevo'
        l = gets.chomp.to_s
      end
    when 2
      system('clear')
      puts 'Login'
      puts 'Ingrese correo'
      l=gets.chomp.to_s
      while !l.include? ('@') and !l.include? ('.com') do
        system('clear')
        puts 'Correo invalido'
        l=gets.chomp.to_s
      end
      login = Login.where(email: l)
      l=gets.chomp.to_s
      while l!=login.password do
        system('clear')
        puts 'La contrasena es incorrecta, intente de nuevo'
        l=gets.chomp.to_s
      end
      query_search
    when 3
      system('clear')
      puts 'Logoff......'
      system('clear')
    else

    end
  end
  def query_search
    puts '1-Buscar Bill of Lading'
    puts '2-Mostrar Bill of Lading por Naviera'
    puts '3-Contenedores por tipo'
    puts '4-10 Paises con puerto ocupado'
    opc = gets.chomp.to_i
    case opc
    when 1


    when 2

    when 3

    when 4

    else

    end
  end
  def country_10
    BlMaster.group('port_of_delivery_id').order('count_id desc').limit(10).count('id')
  end
end
