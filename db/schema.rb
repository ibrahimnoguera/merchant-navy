# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_14_120804) do

  create_table "bl_houses", force: :cascade do |t|
    t.string "bl_code"
    t.integer "bl_master_id"
    t.string "consignee"
    t.integer "num_containers"
    t.index ["bl_master_id"], name: "index_bl_houses_on_bl_master_id"
  end

  create_table "bl_houses_containers", id: false, force: :cascade do |t|
    t.integer "bl_house_id"
    t.integer "container_id"
    t.index ["bl_house_id"], name: "index_bl_houses_containers_on_bl_house_id"
    t.index ["container_id"], name: "index_bl_houses_containers_on_container_id"
  end

  create_table "bl_masters", force: :cascade do |t|
    t.integer "shipping_company_id"
    t.integer "consignee_id"
    t.integer "notify_id"
    t.string "bl_code"
    t.integer "num_ref"
    t.integer "route_id"
    t.integer "port_of_loading_id"
    t.integer "cargo_ship_id"
    t.integer "port_of_delivery_id"
    t.integer "num_containers"
    t.integer "total_weight"
    t.integer "cargo_volume"
    t.integer "country_id"
    t.date "date"
    t.date "estimated_arrival_date"
    t.index ["cargo_ship_id"], name: "index_bl_masters_on_cargo_ship_id"
    t.index ["consignee_id"], name: "index_bl_masters_on_consignee_id"
    t.index ["country_id"], name: "index_bl_masters_on_country_id"
    t.index ["notify_id"], name: "index_bl_masters_on_notify_id"
    t.index ["port_of_delivery_id"], name: "index_bl_masters_on_port_of_delivery_id"
    t.index ["port_of_loading_id"], name: "index_bl_masters_on_port_of_loading_id"
    t.index ["route_id"], name: "index_bl_masters_on_route_id"
    t.index ["shipping_company_id"], name: "index_bl_masters_on_shipping_company_id"
  end

  create_table "bl_masters_containers", id: false, force: :cascade do |t|
    t.integer "bl_master_id"
    t.integer "container_id"
    t.index ["bl_master_id"], name: "index_bl_masters_containers_on_bl_master_id"
    t.index ["container_id"], name: "index_bl_masters_containers_on_container_id"
  end

  create_table "cargo_ships", force: :cascade do |t|
    t.string "name"
    t.integer "year"
    t.decimal "length", precision: 3, scale: 2
    t.decimal "beam", precision: 2, scale: 2
    t.integer "capacity"
    t.integer "weight"
    t.integer "shipping_company_id"
    t.integer "country_id"
    t.decimal "total_weight", precision: 7, scale: 2
    t.index ["country_id"], name: "index_cargo_ships_on_country_id"
    t.index ["shipping_company_id"], name: "index_cargo_ships_on_shipping_company_id"
  end

  create_table "cargos", force: :cascade do |t|
    t.string "item"
    t.integer "quantity"
    t.decimal "weight", precision: 5, scale: 2
  end

  create_table "cargos_containers", id: false, force: :cascade do |t|
    t.integer "cargo_id"
    t.integer "container_id"
    t.index ["cargo_id"], name: "index_cargos_containers_on_cargo_id"
    t.index ["container_id"], name: "index_cargos_containers_on_container_id"
  end

  create_table "consignees", force: :cascade do |t|
    t.string "name"
    t.integer "country_id"
    t.integer "private_client_id"
    t.index ["country_id"], name: "index_consignees_on_country_id"
    t.index ["private_client_id"], name: "index_consignees_on_private_client_id"
  end

  create_table "containers", force: :cascade do |t|
    t.string "code"
    t.integer "country_id"
    t.integer "nvocc_id"
    t.integer "cargo_ship_id"
    t.integer "total_weight"
    t.index ["cargo_ship_id"], name: "index_containers_on_cargo_ship_id"
    t.index ["country_id"], name: "index_containers_on_country_id"
    t.index ["nvocc_id"], name: "index_containers_on_nvocc_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "acronym"
  end

  create_table "logins", force: :cascade do |t|
    t.string "email"
    t.string "password"
  end

  create_table "nvoccs", force: :cascade do |t|
    t.integer "license_number"
    t.string "name"
    t.text "address"
    t.integer "country_id"
    t.string "phone"
    t.index ["country_id"], name: "index_nvoccs_on_country_id"
  end

  create_table "nvoccs_private_clients", id: false, force: :cascade do |t|
    t.integer "nvocc_id"
    t.integer "private_client_id"
    t.index ["nvocc_id"], name: "index_nvoccs_private_clients_on_nvocc_id"
    t.index ["private_client_id"], name: "index_nvoccs_private_clients_on_private_client_id"
  end

  create_table "ports", force: :cascade do |t|
    t.string "port_code"
    t.string "name"
    t.integer "country_id"
    t.index ["country_id"], name: "index_ports_on_country_id"
  end

  create_table "private_clients", force: :cascade do |t|
    t.string "company_name"
    t.text "address"
    t.integer "owner_id"
    t.integer "country_id"
    t.index ["country_id"], name: "index_private_clients_on_country_id"
    t.index ["owner_id"], name: "index_private_clients_on_owner_id"
  end

  create_table "routes", force: :cascade do |t|
    t.integer "port_of_loading_id"
    t.integer "port_of_discharge_id"
    t.index ["port_of_discharge_id"], name: "index_routes_on_port_of_discharge_id"
    t.index ["port_of_loading_id"], name: "index_routes_on_port_of_loading_id"
  end

  create_table "shipping_companies", force: :cascade do |t|
    t.string "name"
    t.integer "country_id"
    t.index ["country_id"], name: "index_shipping_companies_on_country_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.date "date_of_birth"
    t.string "e_mail"
    t.text "address"
    t.integer "national_id_number"
    t.integer "country_id"
    t.index ["country_id"], name: "index_users_on_country_id"
  end

end
