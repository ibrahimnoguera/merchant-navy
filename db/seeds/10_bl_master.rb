require 'faker'

class Filler
  def bl_master_create
    x = 1
    shipping = ShippingCompany.all
    ship = CargoShip.all
    195.times do

      sp = []
      sp << shipping.sample
      shipping -= sp

      consignee = Consignee.all.sample.name
      notify = User.all.sample.name
      bl_code = "BL-" + (rand(899999) + 100000).to_s
      num_ref = (rand(899999) + 100000)
      route = Route.all.sample
      port_of_loading = route.port_of_loading_id
      port_of_delivery = route.port_of_discharge_id

      shp = []
      shp << ship.first
      ship -= shp

      num_containers = Container.where(cargo_ship_id: x).count(:id)

      total_weight = shp[0].weight + shp[0].total_weight

      cargo_volume = num_containers * 39

      country = Country.all.sample.name

      date = Faker::Date.birthday(2)

      estimated_arrival_date = Faker::Date.birthday(1)

      BlMaster.create(shipping_company_id: ShippingCompany.where(name: sp[0].name).ids[0], consignee_id: Consignee.where(name: consignee).ids[0], notify_id: User.where(name: notify).ids[0], bl_code: bl_code, num_ref: num_ref, route_id: route.id, port_of_loading_id: port_of_loading, cargo_ship_id: CargoShip.where(name: shp[0].name).ids[0], port_of_delivery_id: port_of_delivery, num_containers: num_containers, total_weight: total_weight, cargo_volume: cargo_volume, country_id: Country.where(name: country).ids[0], date: date, estimated_arrival_date: estimated_arrival_date)

      x += 1

      sp = []
      shp = []
    end
  end
  def containers_of_bl
    x = 1
    BlMaster.all.length.times do
      bl = BlMaster.find(x)
      bl.containers = Container.where(cargo_ship_id: bl.cargo_ship_id)
      x+=1
    end
  end
end
#Filler.new.bl_master_create
#Filler.new.containers_of_bl
