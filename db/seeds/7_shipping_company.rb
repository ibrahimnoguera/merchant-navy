require 'spreadsheet'
class Filler
  def shipping_fill
    workbook = Spreadsheet.open './db/seeds/shipping.xls'
    worksheets = workbook.worksheets
    cnty=Country.all
    worksheets.each do |worksheet|
      worksheet.rows.each do |row|
        row_cells = row
        ShippingCompany.create(name: row_cells[0], country_id: "#{Country.where(name: row_cells[1]).ids[0]}")
      end
    end
  end
end
#Filler.new.shipping_fill
