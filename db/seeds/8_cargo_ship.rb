require 'spreadsheet'
class Filler
  def ship_fill
    workbook = Spreadsheet.open './db/seeds/cargo_ships.xls'
    worksheets = workbook.worksheets
    cnty=Country.all
    worksheets.each do |worksheet|
      worksheet.rows.each do |row|
        row_cells = row
        ship=ShippingCompany.all.sample.name
        CargoShip.create(name: row_cells[1], year: row_cells[0], length: row_cells[2], beam: row_cells[3], capacity: row_cells[4], weight: row_cells[5], shipping_company_id: "#{ShippingCompany.where(name: ship).ids[0]}", country_id: "#{Country.where(name: row_cells[6]).ids[0]}")
      end
    end
  end
  def weight_add
    x=1
    CargoShip.all.length.times do
      cs = CargoShip.find(x)
      cs.total_weight = Container.where(cargo_ship_id: x).sum(:total_weight)
      cs.save
      x+=1
    end
  end
end
#Filler.new.ship_fill
#Filler.new.weight_add
