require 'faker'

class Filler
  def client_create
    usr=User.all
    1.times do
      company_name = Faker::Company.unique.name
      country = Faker::Address.country
      address = Faker::Address.street_address + ", " + Faker::Address.street_name + ", " + Faker::Address.city + ", " + country
      owner=usr.sample
        PrivateClient.create(company_name: company_name, address: address, owner_id: User.where(name: owner.name, last_name: owner.last_name).ids[0], country_id: "#{Country.where(name: country).ids[0]}")
    end
  end
end
#Filler.new.client_create
