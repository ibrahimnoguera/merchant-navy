class Filler
  def bl_house_create
    x = 1
    BlMaster.all.length.times do
      a = 0
      bl = BlMaster.find(x)
      con = bl.containers
      bl.containers.count.times do
        bl_code = bl.bl_code + "-#{a+1}"
        if con[a].nvocc_id.nil?
          consignee = Country.where(id: con[a].country_id)[0].name
        else
          consignee = Nvocc.where(id: con[a].nvocc_id)[0].name
        end
        num_containers = 1
        bl_master_id = bl.id
        BlHouse.create(bl_code: bl_code, bl_master_id: bl_master_id, consignee: consignee, num_containers: num_containers)
        a+=1
      end
      x+=1
      a = 0
    end
  end
  def containers_of_bl
    x = 1
    BlMaster.all.length.times do
      blm = BlMaster.find(x)
      con = blm.containers
      blh = BlHouse.where(bl_master_id: blm.id)
      a = 0
      con.count.times do
        blh[a].containers = [con[a]]
        a += 1
      end
      a = 0
      x+=1
    end
  end
end
#Filler.new.bl_house_create
#Filler.new.containers_of_bl
