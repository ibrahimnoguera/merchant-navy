require 'spreadsheet'
class Filler
  def nvocc_fill
    workbook = Spreadsheet.open './db/seeds/NVOCC.xls'
    worksheets = workbook.worksheets
    cnty=Country.all
    worksheets.each do |worksheet|
      worksheet.rows.each do |row|
        row_cells = row
        Nvocc.create(license_number: row_cells[0], name: row_cells[1], address: row_cells[2], country_id: "#{Country.where(name: row_cells[3]).ids[0]}", phone: row_cells[4])
      end
    end
  end
  def nvocc_client
    x = 1
    a = []
    pc = PrivateClient.all
    Nvocc.all.length.times do
      2.times do
        b = []
        b << pc.sample
        pc -= b
        a << b[0]
      end
      nv = Nvocc.find(x)
      nv.private_clients = a
      a =[]
      x += 1
    end
  end
end
#Filler.new.nvocc_fill
#Filler.new.nvocc_client
