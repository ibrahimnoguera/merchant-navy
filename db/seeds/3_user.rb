require 'faker'

class Filler
  def user_create
    cnty=Country.all
    100.times do
      name = Faker::Name.unique.first_name
      last_name = Faker::Name.unique.last_name
      date_of_birth = Faker::Date.birthday(18, 65)
      e_mail = Faker::Internet.free_email(name)
      country = Faker::Address.country
      address = Faker::Address.street_address + ", " + Faker::Address.street_name + ", " + Faker::Address.city + ", " + country
      national_id_number=rand(90000000)+9000000
        User.create(name: name, last_name: last_name, date_of_birth: date_of_birth, e_mail: e_mail, address: address, national_id_number: national_id_number, country_id: "#{Country.where(name: country).ids[0]}")
    end
  end
end
#Filler.new.user_create
