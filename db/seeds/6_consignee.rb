require 'faker'

class Filler
  def consignee_create
    cnty=Country.all
    prv=PrivateClient.all
    100.times do
      r=rand(2)
      name = Faker::Company.unique.name
      if r==0 then
        country = Faker::Address.country
        Consignee.create(name: name, country_id: "#{Country.where(name: country).ids[0]}")
      else
        private_client = prv.sample.company_name
        Consignee.create(name: name, private_client_id: PrivateClient.where(company_name: private_client).ids[0])
      end
    end
  end
end
#Filler.new.consignee_create
