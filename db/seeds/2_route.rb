class Filler
  def routing
    port = Port.all
    100.times do
      Route.create(port_of_loading_id: "#{port.ids.sample}", port_of_discharge_id: "#{port.ids.sample}")
    end
  end
end
#Filler.new.routing
