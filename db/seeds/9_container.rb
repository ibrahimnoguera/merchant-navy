require 'spreadsheet'
class Filler
  def acronyms
    workbook = Spreadsheet.open './db/seeds/containers.xls'
    worksheets = workbook.worksheets
    a = []
    cgo = Cargo.all
    35.times do
      worksheets.each do |worksheet|
        worksheet.rows.each do |row|
          row_cells = row
          r=rand(2)
          ship=CargoShip.all.sample.name
          5.times do
            b = []
            b << cgo.sample
            cgo -= b
            a << b[0]
          end
          if r==0 then
            cnty=Country.all.sample.name
            Container.create(code: row_cells[0]+(rand(89999999999)+10000000000).to_s, country_id: "#{Country.where(name: cnty).ids[0]}", cargo_ship_id: CargoShip.where(name: ship).ids[0], cargos: a)
          else
            nvo=Nvocc.all.sample.name
            Container.create(code: row_cells[0]+(rand(89999999999)+10000000000).to_s, nvocc_id: Nvocc.where(name: nvo).ids[0], cargo_ship_id: CargoShip.where(name: ship).ids[0], cargos: a)
          end
          a = []
        end
      end
    end

  end
  def fill_weight
    x = 1
    Container.all.length.times do
      con = Container.find(x)
      con.total_weight = con.cargos.sum(:weight)
      con.save
      x += 1
    end
  end
end
#Filler.new.acronyms
#Filler.new.fill_weight
