require 'faker'

class Filler
  def cargo_create
    cnty=Country.all
    4072.times do
      r = rand(3)
      if r== 0 then
        item = Faker::ElectricalComponents.active
      elsif r == 1 then
        item = Faker::ElectricalComponents.passive
      elsif r == 2 then
        item = Faker::ElectricalComponents.electromechanical
      end
      quantity = rand(100) + 100
      weight = Faker::Number.decimal(2) * quantity
        Cargo.create(item: item, quantity: quantity, weight: weight)
    end
  end
end
#Filler.new.cargo_create
