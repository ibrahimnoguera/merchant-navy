require 'rest-client'
require 'json'

class Fetcher
  attr_accessor :source, :places
  def initialize
    @source = "https://geonode.wfp.org/geoserver/wfs?srsName=EPSG%3A4326&typename=geonode%3Awld_trs_ports_wfp&outputFormat=json&version=1.0.0&service=WFS&request=GetFeature"
  end
  def parser
    @places = JSON.parse(RestClient.get(@source), :symbolize_names => true )
  end
  def matcher
    parser
    @places[:features].each_with_index do |x, num=1| { x => num+=1 }
      Country.create(name: "#{x[:properties][:country]}", acronym: "#{x[:properties][:iso3]}")
      puts "Llenado registro id:#{num}"
    end
  end
end
#Fetcher.new.matcher
