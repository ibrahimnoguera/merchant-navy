class AddTotalWeightToCargoShips < ActiveRecord::Migration[5.2]
  def change
    add_column :cargo_ships, :total_weight, :decimal, precision: 7, scale: 2
  end
end
