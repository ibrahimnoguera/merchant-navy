class CreatePrivateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :private_clients do |t|
      t.string :company_name
      t.text :address
      t.references :owner, foreign_key: {to_table: :users}
      t.references :country, foreign_key: true
    end
  end
end
