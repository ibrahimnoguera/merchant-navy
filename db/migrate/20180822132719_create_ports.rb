class CreatePorts < ActiveRecord::Migration[5.2]
  def change
    create_table :ports do |t|
      t.string :port_code
      t.string :name
      t.references :country, foreign_key: true
    end
  end
end
