class CreateBlHousesContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_houses_containers, id: false do |t|
      t.belongs_to :bl_house, index: true
      t.belongs_to :container, index: true
    end
  end
end
