class CreateCargos < ActiveRecord::Migration[5.2]
  def change
    create_table :cargos do |t|
      t.string :item
      t.integer :quantity
      t.decimal :weight, precision: 5, scale: 2
    end
  end
end
