class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :code
      t.references :country, foreign_key: true
      t.references :nvocc, foreign_key: true
      t.references :cargo_ship, foreign_key: true
      t.integer :total_weight
    end
  end
end
