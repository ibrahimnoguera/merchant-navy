class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :last_name
      t.date :date_of_birth
      t.string :e_mail
      t.text :address
      t.integer :national_id_number
      t.references :country, foreign_key: true
    end
  end
end
