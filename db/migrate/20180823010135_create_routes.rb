class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.references :port_of_loading, foreign_key: {to_table: :ports}
      t.references :port_of_discharge, foreign_key: {to_table: :ports}
    end
  end
end
