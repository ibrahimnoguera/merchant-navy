class CreateBlMaster < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters do |t|
      t.references :shipping_company, foreign_key: true
      t.references :consignee, foreign_key: true
      t.references :notify, foreign_key: {to_table: :users}
      t.string :bl_code
      t.integer :num_ref
      t.references :route, foreign_key: true
      t.references :port_of_loading, foreign_key: {to_table: :ports}
      t.references :cargo_ship, foreign_key: true
      t.references :port_of_delivery, foreign_key: {to_table: :ports}
      t.integer :num_containers
      t.integer :total_weight
      t.integer :cargo_volume
      t.references :country, foreign_key: true
      t.date :date
      t.date :estimated_arrival_date
    end
  end
end
