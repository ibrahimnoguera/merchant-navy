class CreateBlHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_houses do |t|
      t.string :bl_code
      t.references :bl_master, foreign_key: true
      t.string :consignee
      t.integer :num_containers
    end
  end
end
