class CreateNvoccsPrivateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :nvoccs_private_clients, id: false do |t|
      t.belongs_to :nvocc, index: true
      t.belongs_to :private_client, index: true
    end
  end
end
