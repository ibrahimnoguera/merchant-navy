class CreateCargoShips < ActiveRecord::Migration[5.2]
  def change
    create_table :cargo_ships do |t|
      t.string :name
      t.integer :year
      t.decimal :length, precision: 3, scale: 2
      t.decimal :beam, precision: 2, scale: 2
      t.integer :capacity
      t.integer :weight
      t.references :shipping_company, foreign_key: true
      t.references :country, foreign_key: true
    end
  end
end
