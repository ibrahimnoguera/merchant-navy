class CreateConsignees < ActiveRecord::Migration[5.2]
  def change
    create_table :consignees do |t|
      t.string :name
      t.references :country, foreign_key: true
      t.references :private_client, foreign_key: true
    end
  end
end
