class CreateBlMastersContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters_containers, id: false do |t|
      t.belongs_to :bl_master, index: true
      t.belongs_to :container, index: true
    end
  end
end
