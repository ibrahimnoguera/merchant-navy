class CreateCargoContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :cargos_containers, id: false do |t|
      t.belongs_to :cargo, index: true
      t.belongs_to :container, index: true
    end
  end
end
