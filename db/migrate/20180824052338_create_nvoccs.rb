class CreateNvoccs < ActiveRecord::Migration[5.2]
  def change
    create_table :nvoccs do |t|
      t.integer :license_number
      t.string :name
      t.text :address
      t.references :country, foreign_key: true
      t.string :phone
    end
  end
end
